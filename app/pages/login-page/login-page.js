import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';
import { BGADPArmadilloLoginPOST } from '@cells-components/bgadp-armadillo-login';


import { cleanUp } from '../../elements/movements-dm/movements-dm'; 
import '@bbva-web-components/bbva-notification-message';

class LoginPage extends i18n(CellsPage) {
  static get is() {
    return 'login-page';
  }

  static get properties() {
    return {
      host: {
        type: String,
      },
      credentialsError: {
        type: Boolean,
      },
    }
  }
  constructor() {
    super();
    this.host = window.AppConfig.host;
    this.credentialsError = false;
  }
  
  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');
  }

  handleValidation() {
    let canContinue = true;

    [this._userInput, this._passwordInput].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    if (canContinue) 
    {
        this.credentialsError = false;
        const credentials = {
          userId: this._userInput.value,
          password: this._passwordInput.value,
          consumerId: '10000033'
        };
        const session = new BGADPArmadilloLoginPOST({
          host: this.host
        });
        session.htmlContext = this;
        session.generateRequest(false, credentials)
          .then(request => JSON.parse(request.response))
          .then(({ data })=>{
            console.log(data);
            this.publish('user_name', data.userId);
            this.navigate('contactos');
          })
          .catch(()=>
          {
            this.credentialsError = true;
            
          });
    }
  }

  onPageEnter() {
    // Cada vez que accedamos al login, simulamos una limpieza de los datos almacenados en memoria.
    cleanUp();
  }

  onPageLeave() {
    // Cada vez que salgamos del login, limpiamos las cajas de texto.
    setTimeout(() => [this._userInput, this._passwordInput].forEach((el) => el.clearInput()), 3 * 1000);
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
    `;
  }
  render() 
  {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-form-field
            id="user"
            auto-validate
            validate-on-blur 
            label=${this.t('login-page.user-input')}
            required>
          </bbva-form-field>

          <bbva-form-password
            id="password"
            auto-validate
            validate-on-blur 
            label=${this.t('login-page.password-input')}
            required>
          </bbva-form-password>

          <bbva-button-default 
            @click=${this.handleValidation}>
              ${this.t('login-page.button')}
          </bbva-button-default>
          ${this.credentialsError ? html`
            <bbva-notification-message variation="error">
              ${this.t('login-page.credentials-error')}
            </bbva-notification-message>
          `: ''}
        </div>
     </cells-template-paper-drawer-panel>`;
  }
}

window.customElements.define(LoginPage.is, LoginPage);