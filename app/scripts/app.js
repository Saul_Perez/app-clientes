(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'contactos': '/contactos'
    }
  });
}());
