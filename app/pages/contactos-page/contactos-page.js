import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-help-modal';
import '@cells-training/ui-lista-contactos/ui-lista-contactos';

import { getContactos } from '@cells-training/dm-lista-contactos';

class ContactosPage extends i18n(CellsPage) {
  static get is() {
    return 'contactos-page';
  }

  constructor() {
    super();
    this.listaContactos = [];
    this.userName = "Prueba";
    this.userId="";
  }

  static get properties() {
    return {
      userName: { type: String },
      userId: { type: String },
      listaContactos: {type: Array},
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._logoutModal = this.shadowRoot.querySelector('#logoutModal');
  }

  onPageEnter() {
    this.subscribe('user_id', (userId) => this.userId = userId);
   if (!this.listaContactos.length) {
      const { host, requiredToken } = window.AppConfig;
      let config = { host, version: 0, requiredToken, native: false };
      getContactos(this.userId,config, this)
        .then(result => {
          console.log(result);
          this.listaContactos = result;
        })
        .catch(error => {
          console.log("Error al hacer la solicitud");
          console.log(error);
        });
    }
  }

  onPageLeave() {
    this.listaContactos=[];
    console.log("Salio de la página")
  }

  handleCardClick(ev) {
    //const { cardId, cardType } = ev.detail;
    console.log(ev);
    console.log("Entro al evento clic");
  }  

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
        <bbva-header-main
        icon-left1="coronita:on"
        accessibility-text-icon-left1="Cerrar Sesión"
        @header-icon-left1-click=${() => this._logoutModal.open()}
        text=${this.t('contactos-page.welcome.header')}>
      </bbva-header-main>
        </div>
        <div slot="app__main" class="container">
          ${this.listaContactos ? html`<ui-lista-contactos
                                      item-list="${JSON.stringify(this.listaContactos)}"
                                      @item-click="${this.handleCardClick}"
                                      >
                                    </ui-lista-contactos>` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
         
         
          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('contactos-page.logout-modal.header')}
            button-text=${this.t('contactos-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('contactos-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
    `;
  }
}

window.customElements.define(ContactosPage.is, ContactosPage);